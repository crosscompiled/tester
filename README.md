# tester

tester will run various softwares to determine your architecture and supported libc. It will list the compilers which you can use.

## usage

```bash
cd /tmp
wget --no-check-certificate https://gitlab.com/crosscompiled/tester/-/jobs/artifacts/main/raw/run.sh?job=build -O run.sh
wget --no-check-certificate https://gitlab.com/crosscompiled/tester/-/jobs/artifacts/main/raw/binaries.tar.gz?job=build -O binaries.tar.gz
chmod +x run.sh
./run.sh
```
