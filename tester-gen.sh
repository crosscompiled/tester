#!/bin/bash

set -ex

rm -f tester/tester
#rm -rf bin
mkdir -p bin
ignore=(
bootlin--aarch64--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/aarch64-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--aarch64--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/aarch64-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--aarch64--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/aarch64-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--aarch64be--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/aarch64_be-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--aarch64be--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/aarch64_be-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--arcle-750d--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arc-buildroot-linux-uclibc/7.1.1/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--arcle-hs38--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arc-buildroot-linux-gnu/7.1.1/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--arcle-hs38--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arc-buildroot-linux-uclibc/7.1.1/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armebv7-eabihf--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/armeb-buildroot-linux-gnueabihf/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armebv7-eabihf--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/armeb-buildroot-linux-musleabihf/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armebv7-eabihf--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/armeb-buildroot-linux-uclibcgnueabihf/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv5-eabi--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-linux-gnueabi/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv5-eabi--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-linux-musleabi/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv5-eabi--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-linux-uclibcgnueabi/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv6-eabihf--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-linux-gnueabihf/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv6-eabihf--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-linux-musleabihf/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv6-eabihf--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-linux-uclibcgnueabihf/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv7-eabihf--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-linux-gnueabihf/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv7-eabihf--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-linux-musleabihf/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv7-eabihf--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-linux-uclibcgnueabihf/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--armv7m--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/arm-buildroot-uclinux-uclibcgnueabi/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--bfin--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/bfin-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--m68k-68xxx--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/m68k-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--m68k-coldfire--uclibc--bleeding-edge-2018.02-1 # /tool/bin/../libexec/gcc/m68k-buildroot-uclinux-uclibc/7.3.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or director
bootlin--m68k-coldfire--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/m68k-buildroot-uclinux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--microblazebe--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/microblaze-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--microblazebe--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/microblaze-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--microblazebe--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/microblaze-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--microblazeel--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/microblazeel-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--microblazeel--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/microblazeel-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--microblazeel--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/microblazeel-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32el--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mipsel-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32el--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/mipsel-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32el--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mipsel-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32r5el--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/mipsel-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32r5el--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mipsel-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32r6el--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/mipsel-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips32r6el--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mipsel-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips64-n32--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips64-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips64-n32--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips64-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips64-n32--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips64-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips64el-n32--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips64el-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips64el-n32--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips64el-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips64el-n32--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips64el-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips64r6el-n32--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips64el-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--mips64r6el-n32--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/mips64el-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--nios2--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/nios2-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--openrisc--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/or1k-buildroot-linux-musl/5.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--openrisc--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/or1k-buildroot-linux-uclibc/5.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--powerpc-e500mc--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/powerpc-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--powerpc-e500mc--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/powerpc-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--powerpc-e500mc--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/powerpc-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--powerpc64-power8--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/powerpc64-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--powerpc64-power8--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/powerpc64-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--powerpc64le-power8--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/powerpc64le-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--powerpc64le-power8--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/powerpc64le-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--sh-sh4--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/sh4-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--sh-sh4--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/sh4-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--sh-sh4--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/sh4-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--sh-sh4aeb--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/sh4aeb-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--sh-sh4aeb--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/sh4aeb-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--sparc64--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/sparc64-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--sparcv8--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/sparc-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--x86-64-core-i7--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/x86_64-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--x86-64-core-i7--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/x86_64-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--x86-64-core-i7--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/x86_64-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--x86-core2--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/i686-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--x86-core2--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/i686-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--x86-core2--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/i686-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--x86-i686--glibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/i686-buildroot-linux-gnu/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--x86-i686--musl--stable-2018.02-1 # /tool/bin/../libexec/gcc/i686-buildroot-linux-musl/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--x86-i686--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/i686-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
bootlin--xtensa-lx60--uclibc--stable-2018.02-1 # /tool/bin/../libexec/gcc/xtensa-buildroot-linux-uclibc/6.4.0/cc1: error while loading shared libraries: libmpfr.so.4: cannot open shared object file: No such file or directory
)

toolchains=$(<toolchains-latest)
for tool in ${toolchains[@]}; do
    if [ ! -f "./bin/$tool" ]; then
        if [[ ! " ${ignore[*]} " =~ " ${tool} " ]]; then
            prefix=$(docker run --rm -t --entrypoint ls quay.io/crosscompiled/tool:$tool -1 /tool | grep buildroot- | cut -d- -f1)
            docker run --rm -v $(pwd)/tester:/app -w /app -e CC=${prefix}-linux-cc -t --entrypoint make quay.io/crosscompiled/tool:$tool
            mv -f ./tester/tester ./bin/${tool}      
        fi
    fi
done

tar --sort=name -czvf binaries.tar.gz ./bin