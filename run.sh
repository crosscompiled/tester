#!/bin/sh

binsfile=binaries.tar.gz
bins=$(tar -tf "${binsfile}")

rm -rf bin

bins="${bins#./bin/}"
mkdir -p bin
for bin in ${bins}; do
    tar -xf "${binsfile}" "${bin}"
    result=$("${bin}" 2>/dev/null)
    if [ "ok" = "$result" ]
    then
        echo "${bin#./bin/} compiler works"
    fi
    rm "${bin}"
done
